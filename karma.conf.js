// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const path = require('path');

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
      require('karma-spec-reporter')
    ],
    client: {
      captureConsole: false,
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, './coverage/dockerize-it'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['kjhtml', 'spec'],
    htmlReporter: {
      outputFile: 'reports/ut/index.html',
      pageTitle: 'Unit Tests',
      subPageTitle: 'SR-Dashboard',
      groupSuites: true,
      useCompactStyle: true,
      useLegacyStyle: true
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    customLaunchers: {
      ChromeDebugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=19222']
      },
      chromeLocal: {
        base: 'Chrome',
        flags: []
      },
      chromeCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--remote-debugging-port=19222'],
        displayName: 'Chrome (headless)'
      }
    },
    browsers: ['ChromeDebugging'],
    browserNoActivityTimeout: 30000,
    singleRun: false,
    restartOnFileChange: true
  });
};
