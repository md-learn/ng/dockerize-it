# DockerizeIt

## Purpose

- dockerize an Angular application

## How to build it

- a local build

```
npm run build:local
```

- a prod build

```
npm run build:prod
```
